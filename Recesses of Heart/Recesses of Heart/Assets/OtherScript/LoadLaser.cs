﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum laserClass { Normal, HorizontalLasers, VerticalLasers, PlusLazer, XLaser, None }

public class LoadLaser : MonoBehaviour
{
    public laserClass currectClass;
     GameObject laserPrefab;

    private void Start()
    {
        InstantiateLasers();
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

	void Awake()
	{
		laserPrefab =	Resources.Load ("Prefabs/Spells/Laser") as GameObject;
	}
    void InstantiateLasers()
    {
       switch(currectClass)
        {
            //Give me the info on where the player aim does change.
            case laserClass.Normal:
                GameObject laserPrefab01 = Instantiate(laserPrefab, new Vector2(transform.position.x,transform.position.y), 
                    new Quaternion(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z, 0));
                laserPrefab01.transform.SetParent(transform);
                break;
            case laserClass.HorizontalLasers:
                GameObject laserPrefab11 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y),
                    new Quaternion(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z, 0));
                laserPrefab11.transform.SetParent(transform);

                GameObject laserPrefab12 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y),
                    new Quaternion(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z - 180, 0));
                laserPrefab12.transform.SetParent(transform);
                break;
            case laserClass.VerticalLasers:
                GameObject laserPrefab21 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y),Quaternion.identity);
                laserPrefab21.transform.localRotation = Quaternion.Euler(0, 0, 90);
                laserPrefab21.transform.SetParent(transform);

                GameObject laserPrefab22 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y),Quaternion.identity);
                laserPrefab22.transform.localRotation = Quaternion.Euler(0, 0, -90);
                laserPrefab22.transform.SetParent(transform);
                break;
            case laserClass.PlusLazer:
                GameObject laserPrefab31 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab31.transform.localRotation = Quaternion.Euler(0, 0, 90);
                laserPrefab31.transform.SetParent(transform);

                GameObject laserPrefab32 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab32.transform.localRotation = Quaternion.Euler(0, 0, -90);
                laserPrefab32.transform.SetParent(transform);

                GameObject laserPrefab33 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab33.transform.localRotation = Quaternion.Euler(0, 0, 0);
                laserPrefab33.transform.SetParent(transform);

                GameObject laserPrefab34 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab34.transform.localRotation = Quaternion.Euler(0, 0, 180);
                laserPrefab34.transform.SetParent(transform);
                break;
            case laserClass.XLaser:
                GameObject laserPrefab41 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab41.transform.localRotation = Quaternion.Euler(0, 0, 135);
                laserPrefab41.transform.SetParent(transform);

                GameObject laserPrefab42 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab42.transform.localRotation = Quaternion.Euler(0, 0, -45);
                laserPrefab42.transform.SetParent(transform);

                GameObject laserPrefab43 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab43.transform.localRotation = Quaternion.Euler(0, 0, 45);
                laserPrefab43.transform.SetParent(transform);

                GameObject laserPrefab44 = Instantiate(laserPrefab, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                laserPrefab44.transform.localRotation = Quaternion.Euler(0, 0, -135);
                laserPrefab44.transform.SetParent(transform);
                break;
            default:
                break;

        }
    }
}
