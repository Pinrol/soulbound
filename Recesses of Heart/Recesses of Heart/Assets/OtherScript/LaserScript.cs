﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    [SerializeField]
    private Sprite[] LaserHit;
    [SerializeField]
    private Color[] ColorLoop;
    private float timmer;
    public float longevity;

    private int counter;


    //LaserType
    public bool hitEnemy;

    private void LateUpdate()
    {
        this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.Lerp(ColorLoop[0], ColorLoop[1], Mathf.PingPong(Time.time, 0.25F));
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(ColorLoop[0], ColorLoop[1], Mathf.PingPong(Time.time, 0.25F));
        timmer += Time.deltaTime;
        if (timmer > longevity && this.gameObject.transform.localScale.y > 0)
        {
            this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y - 0.1F);
            print("OOf");
        }
        if (this.gameObject.transform.localScale.y <= 0)
        {
            Destroy(this.gameObject.transform.parent.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Wall")
        {
            GetComponent<Animator>().speed = 0;
            StartCoroutine("SwitchSprite");
        }
        if (target.tag == "Mob")
        {
            //Damage
        }
    }

    private IEnumerator SwitchSprite()
    {
        GetComponent<SpriteRenderer>().sprite = LaserHit[counter];
        if (counter == 3)
        {
            counter = -1;
        }
        counter++;
        print(counter);

        yield return new WaitForSeconds(0.1F);
        StartCoroutine("SwitchSprite");
    }
}
