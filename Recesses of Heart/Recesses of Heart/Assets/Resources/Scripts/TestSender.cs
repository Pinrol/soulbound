﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSender : MonoBehaviour {

    public Vector3[] positionIndex = new Vector3[3];
    
    public GameObject activeUnit;
    GameObject managers;
    GameObject arrow;


    private void Start()
    {
        managers = GameObject.Find("Managers");
        arrow = managers.GetComponent<BattleManager>().arrow;
        if (activeUnit == null)
        {
            activeUnit = managers.GetComponent<BattleManager>().activePlayerUnit;
        }
    }
    void Update()
    {
        if (positionIndex[0] != Vector3.zero && managers.GetComponent<BattleManager>().interactWithPanel)
            SetDirection();
        else
            for(int i = 0; i < positionIndex.Length; i++)
            {
                positionIndex[i] = Vector3.zero;
            }
        if(Vector3.Distance(positionIndex[0], positionIndex[2]) < 80 && positionIndex[1] == Vector3.zero)
        {
            for (int i = 0; i < positionIndex.Length; i++)
            {
                arrow.transform.localPosition = Vector3.one * 1000;
                positionIndex[i] = Vector3.zero;
            }
        }
    }
    public void SetDirection()
    {
        
        //Debug.Log("in set direction");
        Vector3 shipPos = positionIndex[0];
        Vector3 shipDir = positionIndex[1] - shipPos;
        Vector3 normShipDir = shipDir.normalized;
        float shipAngle = Mathf.Atan2(-shipDir.y, -shipDir.x) * Mathf.Rad2Deg;
        arrow.transform.rotation = Quaternion.AngleAxis(shipAngle, Vector3.forward);
        if(positionIndex[2] != Vector3.zero)
            Debug.Log(Vector3.Distance(positionIndex[0], positionIndex[2]));
        if (positionIndex[2] != Vector3.zero && Vector3.Distance(positionIndex[0], positionIndex[2]) > 80)
        {
            activeUnit.GetComponent<PlayerMovement>().Launch(shipDir.normalized *-1);
            Debug.Log("remove all things");

            
            for(int i = 0; i < positionIndex.Length; i++)
            {
                positionIndex[i] = Vector3.zero;
            }
            GameObject.Find("Managers").GetComponent<BattleManager>().waitingForPlayerUnit = false;
        }
        else if (Vector3.Distance(positionIndex[0], positionIndex[1]) < 80)
        {
            /*for (int i = 0; i < positionIndex.Length; i++)
            {
                positionIndex[i] = Vector3.zero;
            }*/
            Debug.Log("in else");
            arrow.transform.localPosition= Vector3.one * 1000;
            if(positionIndex[2] != Vector3.zero)
            {
                for (int i = 0; i < positionIndex.Length; i++)
                {
                    positionIndex[i] = Vector3.zero;
                }
            }
            
        }
        else if(positionIndex[1] != Vector3.zero)
        {
            arrow.transform.localPosition = new Vector3(0,0,3);
            float arrowDist = (Vector3.Distance(positionIndex[0], positionIndex[1] ) / 100);
            
            arrow.transform.localScale =new Vector3(arrowDist, 1, 1);
        }
        

    }
}
