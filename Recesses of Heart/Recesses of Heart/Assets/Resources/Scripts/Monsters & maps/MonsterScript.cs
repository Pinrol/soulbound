﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class AttackData
{
	public SpellType Spell;
	public TextMesh cdText;
	public int cd;
	[HideInInspector]
	public int curentCd;
}

[System.Serializable]
public class Attack
{
	public AttackData[] attacks;
}


public class MonsterScript : MonoBehaviour {


[SerializeField]
	private AttackData[] myAttacks;

	public GameObject hpbarNormal, hpbarBoss;

	public int hp, maxhp;

	public int damage = 5;
	public bool Alive = true;
	 
	public bool bossHPbar = false;

	// Use this for initialization
	void Start () {

		hpbarBoss = GameObject.Find ("bossHp");
		hp = maxhp;
		//hpbarNormal = transform.GetChild (2).gameObject;
		int i =0;

		foreach (AttackData ad in myAttacks) 
		{
			
			if (ad.cd  == 0) 
			{
				ad.cd  = Random.Range (1, 5);
			}
			ad.curentCd = ad.cd;
			ad.cdText.text = ""+ ad.curentCd ;
			i++;
		}
	}

	public void TakeDamage( int dmg)
	{
		hp -= dmg;
		if (bossHPbar) {
			hpbarBoss.transform.localScale = new Vector3( ((float)hp / (float)maxhp) ,1f, 1f);
		}
		else 
		{
		hpbarNormal.transform.localScale = new Vector3( ((float)hp / (float)maxhp) ,1f, 1f);
		//rumble animation awwwoo 
		}
		if (hp <= 0) {
			Death ();
		}
	}
	public void Death()
	{
		Alive = false;
        BattleManager.instance.StopFloating();
		gameObject.SetActive (false);
		//death poof animatiomn
		// yes hello im dead
		BattleManager.instance.deadbodies.Add(this);
		BattleManager.instance.enemybodies.Remove(this);
	}

	public void MonsterTurnCountDown()
	{

		if (!Alive) 
		{
			return;
		}
		foreach (AttackData ad in myAttacks) 
		{
			ad.curentCd -= 1;
			ad.cdText.text = ""+ad.curentCd;


		}
			


	}


	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			PlayerMovement pm =
				other.gameObject.GetComponent<PlayerMovement> ();
			if (other.gameObject.GetComponent<Rigidbody2D>().velocity == new Vector2(0f,0f) ) 
			{
				Vector3 dir  = (  0.2f * (other.gameObject.transform.position - this.transform.position )).normalized;

				other.gameObject.transform.Translate ( dir *Time.deltaTime);
			}
		}
			
	}


	public bool MonsterTurnExecute()
	{
		bool attacked = false;
		if (!Alive) 
		{
			return (attacked);
		}
		foreach (AttackData ad in myAttacks) 
		{
			if (ad.curentCd  <= 0) 
			{
				SpellIndex.Instance.CastSpell (ad.Spell,this.gameObject,null);
				ad.curentCd  =ad.cd;
				ad.cdText.text = "" + ad.curentCd ;
				attacked = true;

			}



		}

		return (attacked);
	}



	// Update is called once per frame
	void Update () {
		
	}
}
