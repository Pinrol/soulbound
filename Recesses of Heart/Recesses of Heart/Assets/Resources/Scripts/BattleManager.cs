﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour {


    [SerializeField]
	List<PlayerMovement> playerBoddies = new List<PlayerMovement>();
    [SerializeField]
	int playerHP,playerunitTurn = 0;
	public bool waitingForPlayerUnit = false;
    [SerializeField]
    GameObject BigTouchPanel;
    [SerializeField]
	public  List<MonsterScript> enemybodies = new List<MonsterScript>();
	public  List<MonsterScript> deadbodies = new List<MonsterScript>();
    private Transform[] monsterpos;
    public GameObject activePlayerUnit;
    public bool interactWithPanel = false;
    public GameObject arrow;
    public GameObject unitHighlight;
    // Use this for initialization

	public Spawner waveHandler;
	public static BattleManager instance;

    private void Awake()
    {
		instance = this;
        activePlayerUnit = playerBoddies[0].gameObject;
        if (!arrow)
        {
            arrow = (GameObject)Instantiate(Resources.Load("Prefabs/Arrow"));
        }
        arrow.transform.position = Vector3.one * 1000;
        if (!unitHighlight)
        {
            unitHighlight = (GameObject)Instantiate(Resources.Load("Prefabs/UnitHighlight"));
        }
        unitHighlight.transform.position = Vector3.one * 10000;
    }

    void Start () {
		BigTouchPanel = GameObject.Find("BigTouchPanel");
        waitingForPlayerUnit = true;
        
        StartCoroutine(Battle());
    }



	IEnumerator Battle()
	{
        Debug.Log("in battle");
		int turncount = 0;
        playerunitTurn = 0;

        foreach (MonsterScript enemy in enemybodies)
        { }

        
        waveHandler.NextWave ();
        StartCoroutine("FloatingSprite");
        while (playerHP > 0 ) // && enemies.count > 0 
		{


			if (enemybodies.Count == 0) 
			{
                StopCoroutine("FloatingSprite");
                waveHandler.NextWave ();
				Debug.Log ("Next Wave");
                
                StartCoroutine("FloatingSprite");
                yield return new WaitForSeconds (1f);

			}

			foreach (PlayerMovement player in playerBoddies) {
				player.bumped = false;
			}

            waitingForPlayerUnit = true;
            Debug.Log("Start of loop");
            // playerBoddies[ playerunitTurn]. Activate!! 
            interactWithPanel = true;

            BigTouchPanel.GetComponent<TestSender>().activeUnit = playerBoddies[playerunitTurn].gameObject;
            

            activePlayerUnit = playerBoddies[playerunitTurn].gameObject;
		
            arrow.transform.parent = activePlayerUnit.transform;

            arrow.transform.localScale = Vector3.one;
            unitHighlight.transform.parent = activePlayerUnit.transform;
            unitHighlight.transform.localPosition = Vector3.zero;

            ///arrow.transform.localPosition = Vector3.zero;
            while (waitingForPlayerUnit)
            {
                yield return null;
            }
            arrow.transform.localPosition = Vector3.zero;
            unitHighlight.transform.parent = null;
            unitHighlight.transform.position = Vector3.one * 1000;
            arrow.transform.parent = null;
            arrow.transform.position = Vector3.one * 1000;
            

		    
			waitingForPlayerUnit = true;
            interactWithPanel = false;
            Debug.Log("waiting for slowdown");
            while (waitingForPlayerUnit == true)
			{
                
				yield return null;
			}
            Debug.Log("slowdown happened");
            playerunitTurn++;
            if (playerunitTurn >= playerBoddies.Count)
            {
                playerunitTurn = 0;
            }
		
            Debug.Log("enemy turn");
            // Enemies turn!
            yield return new WaitForEndOfFrame();
			foreach(MonsterScript enemy in enemybodies)
            {
				
				if (enemy.MonsterTurnExecute () == true) 
				{
					yield return new WaitForSeconds (0.5f);
				}
            }

			foreach (MonsterScript enemy in enemybodies)
            {
                enemy.MonsterTurnCountDown();
            }

            //BigTouchPanel.GetComponent<fuuuk>().EgenFuckingMetod();
            
            interactWithPanel = true;
            turncount++;
            Debug.Log("end");
            waitingForPlayerUnit = true;
			yield return null;
		}


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PierceTurn( MovmentType type)
	{
		bool pierce = true;
		if (type == MovmentType.bounce) {
			pierce = false;
		}

		foreach(MonsterScript ms in enemybodies)
		{
			ms.gameObject.GetComponent<BoxCollider2D> ().isTrigger = pierce;
		}
	}


    private void SetUnitInteraction(bool b)
    {
        BigTouchPanel.GetComponent<BoxCollider>().enabled = b;
        Debug.Log("set interaction = " + b);
    }


     public void DisplaceSprite(Vector3 collpos, Transform body)
    {
        //StopCoroutine("LerpBackSprite");
        //body.GetChild(0).localPosition = Vector3.zero;


        Vector3 tempPos = body.position - collpos;
        body.GetChild(0).localPosition = tempPos/8;

        //StartCoroutine(LerpBackSprite(0.5f, body, tempPos/8));
        //StartCoroutine(FloatingSprite());
        
    }

    IEnumerator LerpBackSprite(float delay, Transform body, Vector3 pos)
    {
        float starttime = Time.time;
        while (Time.time < starttime + delay)
        {
            Debug.Log("in lerp");
            body.GetChild(0).localPosition = Vector3.MoveTowards(body.GetChild(0).localPosition, Vector3.zero, Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }


    }
    IEnumerator FloatingSprite()
    {
        Debug.Log("in floatingsprite");
        monsterpos = new Transform[enemybodies.Count];
        var xx = enemybodies.ToArray();
        Debug.Log(enemybodies.Count);
        for(int i = 0; i < enemybodies.Count; i++)
        {
            monsterpos[i] = (xx[i].gameObject.transform);
            Debug.Log(i);
        }
        Vector3[] positionIndex = new Vector3[monsterpos.Length];
        for(int i = 0; i < monsterpos.Length; i++)
        {
            positionIndex[i] = xx[i].transform.position;
            Debug.Log("thiiiiiis, " + positionIndex[i]);
        }
        Debug.Log(positionIndex);
        while (true)
        {
            //monsterpos[0].GetChild(0).transform.position = Vector3.MoveTowards(monsterpos[0].GetChild(0).transform.position, new Vector3(monsterpos[0].position.x, Mathf.Sin(Time.time * 3) / 8 + positionIndex[0].y, monsterpos[0].position.z), Time.deltaTime/8);
            
            for (int i = 0; i < monsterpos.Length; i++)
            {
                //Debug.Log("this is i: " + i);
                if (monsterpos.Length != 0)
                    monsterpos[i].GetChild(0).transform.position = Vector3.MoveTowards(monsterpos[i].GetChild(0).transform.position, new Vector3(monsterpos[i].position.x, Mathf.Sin(Time.time * 3) / 8 + positionIndex[i].y, monsterpos[i].position.z), Time.deltaTime / 3);
                else
                    StopCoroutine(FloatingSprite());
            }
            yield return new WaitForEndOfFrame();
        }
        
    }

    public void StopFloating()
    {
        StopCoroutine(FloatingSprite());
    }

}
