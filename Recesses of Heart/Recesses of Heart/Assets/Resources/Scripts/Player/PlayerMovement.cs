﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MovmentState
{
	idle, moving
}
public enum MovmentType
{
	bounce, Pierce
}
public enum ParticleType
{
    wall, mob, idk, räckerförtillfället
}

public class PlayerMovement : MonoBehaviour {

	public SpellType bumpSpell;

	public MovmentState mState = MovmentState.idle;
	public MovmentType type = MovmentType.bounce;

	public float StartSpeed = 400;

	public float curentMagnitude;
	[SerializeField]float amountDrag = 0.02f;
	public Vector3 MovementDirection;
	public Rigidbody2D rb;

	public bool bumped = false;

	public bool barrierImune = false;

	public int attackPower;
	// Use this for initialization
	void Start () {
		rb = this.GetComponent<Rigidbody2D>();
		this.GetComponent<CircleCollider2D> ().isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.X)) 
		{
			Launch (new Vector2(Random.Range(-1f,1f),Random.Range(-1f,1f)));
		}
		if (Input.GetKeyDown (KeyCode.C)) 
		{
			transform.position = new Vector3 (0,0,0);
		}
	}
		

	public void Launch(Vector2 direction)
	{
		this.GetComponent<CircleCollider2D> ().isTrigger = false;
		MovementDirection = direction;

		BattleManager.instance.PierceTurn (type);
	

		rb.drag = 0.02f;
		mState = MovmentState.moving;
		rb.AddForce (direction * StartSpeed);
	
	   StartCoroutine (Movement());


	}

	IEnumerator Movement()
	{
		
		yield return new WaitForSeconds(3f);
		while (mState == MovmentState.moving) 
		{
			curentMagnitude = rb.velocity.magnitude;
			if (rb.velocity.magnitude < 8f ) 
			{
				
				rb.drag = 0.8f;
				mState = MovmentState.idle;
                Debug.Log("setting waiting = false");
				yield return new WaitForSeconds(1f);
				this.GetComponent<CircleCollider2D> ().isTrigger = true;
				rb.velocity = new Vector3(0,0,0);
				GameObject.Find("Managers").GetComponent<BattleManager>().waitingForPlayerUnit = false;
				yield break;
			}

			yield return new WaitForSeconds(1f);
		}

		yield break;
	}


//	void OnColitionEnter2D(Collision2D other)
//	{
//		Debug.Log (other.gameObject.name);
//
//		if ((other.gameObject.tag == "Mob") && mState == MovmentState.moving)
//		{
//
//		if (type == MovmentType.bounce) 
//			{
//				Debug.Log ("DAAAMAGE");
//				other.gameObject.GetComponent<MonsterScript> ().TakeDamage (attackPower);
//			rb.velocity *= 0.80f;
//			//damage pierce
//			}
//		}
//
//	}

	void OnTriggerEnter2D( Collider2D other)
	{


		float velocityX  = rb.velocity.x;
		float velocityY  = rb.velocity.y;
	
	

		switch (other.gameObject.tag) 
		{
		case "Mob":
			if (type == MovmentType.bounce) 
			{
				Debug.Log ("DAAAMAGE");
				other.gameObject.GetComponent<MonsterScript> ().TakeDamage (attackPower);
				rb.velocity *= 0.90f;
                //damage pierce
                StartCoroutine(WallParticle(other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position), ParticleType.mob));
                BattleManager.instance.DisplaceSprite(other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position), other.transform);
                }
                else if (type == MovmentType.Pierce) 
			{
				

				Debug.Log ("DAAAMAGE");
				other.gameObject.GetComponent<MonsterScript> ().TakeDamage (attackPower);
				rb.velocity *= 0.90f;
                //damage pierce
                StartCoroutine(WallParticle(other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position), ParticleType.mob));
                BattleManager.instance.DisplaceSprite(other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position), other.transform);
                }
                break;
		case "Wall":
			if (other.transform.position.y == 0) {
				rb.velocity = new Vector2 ((velocityX * -1), velocityY);
			} 
			else 
			{
				rb.velocity = new Vector2((velocityX ) , velocityY* -1);
			}



            //StartCoroutine(WallParticle ,other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position)), ParticleType.wall);
            StartCoroutine(WallParticle(other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position), ParticleType.wall));

			rb.velocity *= 0.94f;
			//Debug.Log ("wall");
			//GetComponent<Rigidbody2D>().drag = 0.1f;

			break;
		

		case "WeakSpot":
			//masiveDmg
			break;

		case "Barrier":
			//masiveDmg
			if (barrierImune == false)
			{
				rb.velocity *= 0.5f;
				EffecktSpawer.Instance.BarrierAflickt (transform.position, other.transform.GetChild(0).GetComponent<SpriteRenderer>().color);
			}
			break;

        case "Block":
            //StartCoroutine(("wallParticle"), (other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position)));
            StartCoroutine(WallParticle(other.gameObject.GetComponent<Collider2D>().bounds.ClosestPoint(transform.position), ParticleType.wall));
            break;

        }


	

	}

    void OnTriggerExit2D(Collider2D other)
    {
        switch (other.gameObject.tag)
        {
            case "Player":

                if (mState == MovmentState.idle && bumpSpell != null && bumped == false)
                {
                    bumped = true;

                    SpellIndex.Instance.CastSpell(bumpSpell, this.gameObject, other.gameObject);
                }

                break;
        }
    }

    private IEnumerator WallParticle(Vector3 pos, ParticleType type)
    {
        switch (type)
        {
            case ParticleType.wall:
                GameObject wallPSystem = (GameObject)Instantiate(Resources.Load("Prefabs/WallParticle"));
                wallPSystem.transform.position = pos + new Vector3(0, 0, -2);
                yield return new WaitForSeconds(0.5f);
                DestroyObject(wallPSystem);
                break;

            case ParticleType.mob:
                GameObject mobPSystem = (GameObject)Instantiate(Resources.Load("Prefabs/MobParticle"));
                mobPSystem.transform.position = pos + new Vector3(0, 0, -2);
                mobPSystem.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360f)));
                yield return new WaitForSeconds(2f);
                DestroyObject(mobPSystem);
                break;


        }
        

    }



}
