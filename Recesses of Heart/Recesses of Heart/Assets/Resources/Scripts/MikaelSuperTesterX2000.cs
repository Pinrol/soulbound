﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MikaelSuperTesterX2000 : MonoBehaviour {


	public List<MonsterScript> testMonsters = new List<MonsterScript>();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown (KeyCode.M)) 
		{
			foreach (MonsterScript mc in testMonsters) 
			{
				mc.MonsterTurnCountDown ();
			}
		}


		if (Input.GetKeyDown (KeyCode.N)) 
		{
			foreach (MonsterScript mc in testMonsters) 
			{
				mc.MonsterTurnExecute ();
			}
		}

	}
}
