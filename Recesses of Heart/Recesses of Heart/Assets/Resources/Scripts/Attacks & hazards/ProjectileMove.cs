﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMove : MonoBehaviour {


	public Vector3 dir, moveTo;
	public float speed;
	// Use this for initialization
	void Start () {
		StartCoroutine (MoveTo(moveTo));
		
	}



	IEnumerator MoveTo(Vector3 pos)
	{

		if (dir == new Vector3(0,0,0)) {
			dir = ( pos - this.transform.position).normalized;

		}
	

		while (true) 
		{
			//Debug.Log ("gogo" + dir);
			transform.Translate (dir * Time.fixedDeltaTime * speed);
			yield return null;
		}

	}

	// Update is called once per frame
	void Update () {
		
	}
}
