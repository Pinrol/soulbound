﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedRemoval : MonoBehaviour {

	public float timer = 1f;


	// Use this for initialization
	void Start () {
		StartCoroutine (RemoveMe()); 
	}



	IEnumerator RemoveMe()
	{
		yield return new WaitForSeconds (timer);
		Destroy (this.gameObject);
	}
	// Update is called once per frame
	void Update () {

	}
}
