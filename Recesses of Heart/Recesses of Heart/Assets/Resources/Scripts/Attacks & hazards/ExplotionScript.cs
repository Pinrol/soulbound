﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionScript : MonoBehaviour {

	public float timer = 1f;
	public int damage = 5;
	public Owener Source = Owener.neutral;

	// Use this for initialization
	void Start () {
		StartCoroutine (Explode()); 
	}

	void OnTriggerEnter2D( Collider2D other)
	{
		if (other.tag == "Player") {
			if (Source == Owener.enemy )
			{
					PlayerStatus.instance.DamageToPlayer (damage * 3);
					}
		}
		else if (other.tag == "Mob" ) 
		{
			if (Source == Owener.player) {
				Debug.Log ("bam bam");
				other.GetComponent<MonsterScript>().TakeDamage(damage *3 );
			}
		}
	}

	IEnumerator Explode()
	{
		yield return new WaitForSeconds (timer);
		Destroy (this.gameObject);
	}
	// Update is called once per frame
	void Update () {
		
	}
}
