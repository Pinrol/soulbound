﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SpellType
{
	Explotion,
	FolloWPiercer,

}


public class SpellIndex : MonoBehaviour {

	private static SpellIndex instance;
	public static SpellIndex Instance
	{
		get { return instance == null ? Instance = new GameObject().AddComponent<SpellIndex>() : instance; }
		private set { instance = value; }
	}


	public void CastSpell(SpellType spell, GameObject caster, GameObject target)
	{
		switch (spell) 
		{
		case SpellType.Explotion:

			Vector3 pos;
			if (target == null) {
				pos = caster.transform.position;
			} else {
				pos = caster.transform.position;
			}
			EffecktSpawer.Instance.SpawnExplotion (ExtractAttack(caster),pos,ExtractOwner(caster));
			break;


		case SpellType.FolloWPiercer:

		
			EffecktSpawer.Instance.FollowPiercerStart (ExtractAttack(caster),target,caster);
			break;
		}

	}


	public int ExtractAttack(GameObject go)
	{
		if (go.tag == "Mob") 
		{
			return go.GetComponent<MonsterScript> ().damage;
		}
		else if (go.tag == "Player") 
		{
			return go.GetComponent<PlayerMovement> ().attackPower;
		}

		return 0;
	}


	public Owener ExtractOwner(GameObject go)
	{
		if (go.tag == "Mob") 
		{
			return Owener.enemy;
		}
		else if (go.tag == "Player") 
		{
			return Owener.player;
		}

		return Owener.neutral;
	}



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
