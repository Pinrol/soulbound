﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour {



	public List<GameObject> waveList = new List<GameObject>();

	 BattleManager bm;

	int index = -1;


	// Use this for initialization
	void Awake () {

		bm = BattleManager.instance;
		BattleManager.instance.waveHandler = this;

	}

	public void NextWave()
	{
		if (index != -1) {
			
			Destroy (waveList[index]);
			
		}

		index++;
		if (index > waveList.Count) 
		{
			Debug.Log ("you win");
			// do stuff
			return;
		}


		BattleManager.instance.enemybodies.Clear();
		BattleManager.instance.deadbodies.Clear ();
	
		waveList[index].SetActive (true);
		foreach( MonsterScript ms in waveList[index].GetComponentsInChildren<MonsterScript>() )
			{
				BattleManager.instance.enemybodies.Add (ms);
			}
			// add to manager


	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
